$().ready(() => {
  $("form").on;
});
const api_key = `273ce96b97655d17c07229c33fa460a5`;

async function getMovie(
  url = `https://api.themoviedb.org/3/movie/now_playing?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US&page=`,
  page = 1
) {
  const response = await fetch(url + page);
  const results = await response.json();
  $("#listFilm").empty();
  let pages = 0;
  if (results.total_pages === undefined) pages = 0;
  if (results.total_pages <= 8 && results.total_pages != undefined) {
    pages = results.total_pages;
  } else pages = 8;

  if (pages >= 0) {
    for (let index = 1; index <= pages; index++) {
      $(".pagination1").append(
        `
      <li class="page-item"><button class="page-link" onclick="return loading(\'${url}${index}\')">${index}</button></li>
      `
      );
    }
  }

  for (result of results.results) {
    show(result);
  }
}
function loading(url) {
  $("#listFilm").empty();
  $("#listFilm").append(`
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>
  `);
  getMoviebyPage(url);
}
async function getMoviebyPage(url) {
  const response = await fetch(url);
  const results = await response.json();
  // console.log(results.total_pages);
  $("#listFilm").empty();

  for (result of results.results) {
    show(result);
  }
}

async function searchMovie() {
  $(".pagination1").empty();
  const input = $("#search").val();
  const url = `https://api.themoviedb.org/3/search/movie?api_key=273ce96b97655d17c07229c33fa460a5&query=${input}&language=en-US&page=`;
  $("#listFilm").empty();
  $("#listFilm").append(`
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>
  `);
  getMovie(url, 1);
}

function show(result) {
  const url_images = `https://image.tmdb.org/t/p/w200`;

  $("#listFilm").append(`
    <div class="col-md-3 card-film">
    <div class="card mb- box-shadow">
      <img
        src="${url_images}${result.poster_path}"
        data-holder-rendered="true"
      />
      <div class="card-body">
        <div
          class="d-flex justify-content-between align-items-center"
        >
          <h6 class="card-title">${
            result.title ? result.title : result.name
          }</h6>
          <small><i class="fas fa-star"></i> ${result.vote_average}</small>
          <button class="detail-button" onclick="getDetailMovie(${
            result.id
          })">Detail</button>
        </div>
      </div>
    </div>
  </div>
</div>
`);
}
getMovie(undefined);

function render(result) {
  const url_images = `https://image.tmdb.org/t/p/w300`;
  const genres = [];
  for (genre of result.genres) {
    genres.push(" " + genre.name);
  }
  const release_date = new Date(result.release_date);
  $(".row").empty();
  $(".row").append(`
  <!-- Portfolio Item Heading -->
  <h1 class="my-4">${result.original_title}</h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-8">
      <img class="img-fluid" src="${url_images}${result.poster_path}" alt="">
    </div>

    <div class="col-md-4">
      <h3 class="my-3">Overview</h3>
        <p>${result.overview}</p>
      <h3 class="my-3">Movie Details</h3>
      <ul>
      <li class="detail-movie">${release_date.toLocaleDateString()}</li>
      <li class="detail-movie">${
        result.vote_average
      } <i class="fas fa-star"></i></li>
        <li class="detail-movie">${result.runtime} minutes</li>
        <li class="detail-movie">${genres}</li>

      </ul>
    </div>

  </div>

  <h3 class="my-4">Director</h3>

  <div class="row" id="director" >

   
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>

  </div>

  <h3 class="my-4">Top Billed Cast</h3>

  <div class="row" id="credits-list" >

   
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>

  </div>

  <h3 class="my-4">Review</h3>

  <div class="row" id="review" >

   
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>

  </div>
  <!-- /.row -->
`);
  getReview(result.id);
}

async function getDetailMovie(Id) {
  url = `https://api.themoviedb.org/3/movie/${Id}?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US`;
  const response = await fetch(url);
  const result = await response.json();
  $(".pagination1").empty();

  render(result);
  getCredits(Id);
}
async function getCredits(Id) {
  url = `https://api.themoviedb.org/3/movie/${Id}/credits?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US`;
  const url_images = `https://image.tmdb.org/t/p/w200`;
  const response = await fetch(url);
  const result = await response.json();
  console.log(result);

  $("#credits-list").empty();
  $("#director").empty();
  $("#director").append(`
  <div class="col-md-3 col-sm-6 mb-4">
      <a href="#">
        <img class="img-fluid cast" src="${url_images}${result.crew[0].profile_path}" alt="">
        <h6>${result.crew[0].name}</h6>
      </a>
    </div>
  `);
  for (let i = 0; i <= 3; i++) {
    let cast = result.cast[i];
    $("#credits-list").append(`
    <div class="col-md-3 col-sm-6 mb-4">
        <a href="#" onclick="return searchPeople(${cast.id})">
          <img class="img-fluid cast" src="${url_images}${cast.profile_path}" alt="">
          <h6>${cast.name}</h6>
        </a>
      </div>
    `);
  }
}

async function searchPeople(Id) {
  const url = `https://api.themoviedb.org/3/person/${Id}?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US`;
  const url_images = `https://image.tmdb.org/t/p/w500`;
  const response = await fetch(url);
  const result = await response.json();
  const birthday = new Date(result.birthday);
  console.log(result);
  const movies = await getMovieCastOfActor(Id);
  $("#listFilm").empty();
  $("#listFilm").append(`
  <!-- Portfolio Item Heading -->
  <h1 class="my-4">${result.name}</h1>

  <!-- Portfolio Item Row -->
  <div class="row">

    <div class="col-md-8">
      <img class="img-fluid" src="${url_images}${result.profile_path}" alt="">
    </div>

    <div class="col-md-4">
     
      <ul>
      <li class="detail-movie">Also Know As: ${result.also_known_as}</li>

      <li class="detail-movie">Birthday: ${birthday.toLocaleDateString()}</li>
      <li class="detail-movie">Gender: ${
        result.gender == "2" ? "Male" : "Female"
      } </li>
        <li class="detail-movie">Place of birth: ${
          result.place_of_birth
        } minutes</li>
        <li class="detail-movie">Deparment: ${result.known_for_department}</li>
        <li class="detail-movie" id="movies">Movies: </li>

      </ul>
    </div>
    <div>
    <h3 class="my-3">Introduction</h3>
    <p>${result.biography}</p>
    </div>
    
  </div>

  `);
  for (movie of movies) {
    $("#movies").append(`
      <span onclick="return getDetailMovie(${movie.id})">${movie.name}, </span>
      `);
  }
}
async function getMovieCastOfActor(Id) {
  const url = `https://api.themoviedb.org/3/person/${Id}/movie_credits?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US`;
  const response = await fetch(url);
  const result = await response.json();
  const movies = [];
  for (let i = 0; i <= 4; i++) {
    movies.push({ name: result.cast[i].original_title, id: result.cast[i].id });
  }
  return movies;
}

async function getMovieByCastName() {
  $(".pagination1").empty();
  const input = $("#search").val();
  const url = `https://api.themoviedb.org/3/search/person?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US&query=${input}&include_adult=false&page=1`;
  const response = await fetch(url);
  const result = await response.json();
  $("#listFilm").empty();
  $("#listFilm").append(`
  <div class="container1">
  <div class="item-1"></div>
  <div class="item-2"></div>
  <div class="item-3"></div>
  <div class="item-4"></div>
  <div class="item-5"></div>
</div>
  `);
  let numberPeople = 0;
  if (result.results.length >= 5) {
    numberPeople = 5;
  } else {
    numberPeople = result.results.length;
  }
  for (let i = 0; i <= numberPeople - 1; i++) {
    for (let j = 0; j <= result.results[i].known_for.length - 1; j++) {
      show(result.results[i].known_for[j]);
    }
  }
}

function searchType(event) {
  event.preventDefault();
  if ($("select[name=type]").val() == 1) {
    searchMovie();
  } else {
    getMovieByCastName();
  }
}

async function getReview(Id) {
  const url = `https://api.themoviedb.org/3/movie/${Id}/reviews?api_key=273ce96b97655d17c07229c33fa460a5&language=en-US&page=1`;
  const response = await fetch(url);
  const result = await response.json();
  let numberReview = 0;
  if (result.results.length <= 3) {
    numberReview = 3;
  } else {
    numberReview = result.results.length;
  }
  $("#review").empty();
  for (let i = 0; i < numberReview; i++) {
    $("#review").append(`
    <div class="review-content">
    <p><strong>${result.results[i].author}</strong>: ${result.results[i].content}</p>
    </div>
    `);
  }
}
